package uz.dilshodjon216.uzmobileussd.modelView

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import uz.dilshodjon216.uzmobileussd.data.Response
import uz.dilshodjon216.uzmobileussd.model.*

class BannerViewModel() : ViewModel() {
    var response = Response()
    fun listenToTariffs(language: String, tabel: String): MutableLiveData<ArrayList<Tariffs>> {
        return response.getTariffs(language, tabel)
    }

    fun listenToUssd(language: String): MutableLiveData<ArrayList<Ussd>> {
        return response.getUssd(language)
    }

    fun listemToXizmatlar(language: String): MutableLiveData<ArrayList<Xizmatlar>> {
        return response.getXizmatlar(language)
    }

    fun listenToMinuts(language: String): MutableLiveData<ArrayList<Minuts>> {
        return response.getMinuts(language)
    }

    fun listenToSMS(language: String):MutableLiveData<ArrayList<SMS>>{
        return response.getSMS(language)
    }
    fun listenToInternet(language: String):MutableLiveData<ArrayList<Internet>>{
        return response.getInternet(language)
    }
    fun listenToNews(language: String):MutableLiveData<ArrayList<News>>{
        return response.getNews(language)
    }

    fun listenToAksiylar(language: String):MutableLiveData<ArrayList<Aksiylar>>{
        return response.getAksiyalar(language)
    }
    fun listenToOpeator(language: String):MutableLiveData<ArrayList<Opetor>>{
        return response.getOpertorlar(language)
    }
}