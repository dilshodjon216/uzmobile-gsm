package uz.dilshodjon216.uzmobileussd.data

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.firestore.FirebaseFirestore
import uz.dilshodjon216.uzmobileussd.model.*
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.TAG

class Response {

    fun getTariffs(language: String, tabel: String): MutableLiveData<ArrayList<Tariffs>> {

        var tariffslist = MutableLiveData<ArrayList<Tariffs>>()
        var tariffslist1 = ArrayList<Tariffs>()
        val db = FirebaseFirestore.getInstance()
        db.collection("uzmobile").document(language).collection(tabel)
            .addSnapshotListener { snapshot, error ->
                if (error != null) {
                    Log.w(TAG, "Listen Failed", error)
                    return@addSnapshotListener
                }
                if (snapshot != null) {
                    val documents = snapshot?.documents
                    documents?.forEach {
                        val tariffs = it.toObject(Tariffs::class.java)
                        if (tariffs != null) {
                            tariffslist1.add(tariffs)
                        }
                    }
                    tariffslist.value = tariffslist1
                }

            }

        return tariffslist
    }

    fun getUssd(language: String): MutableLiveData<ArrayList<Ussd>> {
        var ussdListM = MutableLiveData<ArrayList<Ussd>>()
        var ussdList = ArrayList<Ussd>()
        val db = FirebaseFirestore.getInstance()

        db.collection("uzmobile").document(language).collection("Ussd")
            .addSnapshotListener { snapshot, error ->
                if (error != null) {
                    Log.w(TAG, "Listen Failed", error)
                    return@addSnapshotListener
                }
                if (snapshot != null) {
                    val documents = snapshot?.documents
                    documents?.forEach {
                        val tariffs = it.toObject(Ussd::class.java)
                        if (tariffs != null) {
                            ussdList.add(tariffs)
                        }
                    }
                    ussdListM.value = ussdList
                }

            }

        return ussdListM
    }

    fun getXizmatlar(language: String): MutableLiveData<ArrayList<Xizmatlar>> {
        var ussdListM = MutableLiveData<ArrayList<Xizmatlar>>()
        var ussdList = ArrayList<Xizmatlar>()
        val db = FirebaseFirestore.getInstance()

        db.collection("uzmobile").document(language).collection("Xizmatlar")
            .addSnapshotListener { snapshot, error ->
                if (error != null) {
                    Log.w(TAG, "Listen Failed", error)
                    return@addSnapshotListener
                }
                if (snapshot != null) {
                    val documents = snapshot?.documents
                    documents?.forEach {
                        val tariffs = it.toObject(Xizmatlar::class.java)
                        if (tariffs != null) {
                            ussdList.add(tariffs)
                        }
                    }
                    ussdListM.value = ussdList
                }

            }

        return ussdListM
    }

    fun getMinuts(language: String): MutableLiveData<ArrayList<Minuts>> {
        var minuts = MutableLiveData<ArrayList<Minuts>>()
        var minutsList = ArrayList<Minuts>()
        val db = FirebaseFirestore.getInstance()

        db.collection("uzmobile").document(language).collection("Minutlar")
            .addSnapshotListener { snapshot, error ->
                if (error != null) {
                    Log.w(TAG, "Listen Failed", error)
                    return@addSnapshotListener
                }
                if (snapshot != null) {
                    val documents = snapshot?.documents
                    documents?.forEach {
                        val minuts = it.toObject(Minuts::class.java)
                        if (minuts != null) {
                            minutsList.add(minuts)
                        }
                    }
                    minuts.value = minutsList
                }

            }

        return minuts
    }

    fun getSMS(language: String): MutableLiveData<ArrayList<SMS>> {
        var minuts = MutableLiveData<ArrayList<SMS>>()
        var minutsList = ArrayList<SMS>()
        val db = FirebaseFirestore.getInstance()

        db.collection("uzmobile").document(language).collection("SMS")
            .addSnapshotListener { snapshot, error ->
                if (error != null) {
                    Log.w(TAG, "Listen Failed", error)
                    return@addSnapshotListener
                }
                if (snapshot != null) {
                    val documents = snapshot?.documents
                    documents?.forEach {
                        val minuts = it.toObject(SMS::class.java)
                        if (minuts != null) {
                            minutsList.add(minuts)
                        }
                    }
                    minuts.value = minutsList
                }

            }

        return minuts
    }

    fun getInternet(language: String): MutableLiveData<ArrayList<Internet>> {
        var minuts = MutableLiveData<ArrayList<Internet>>()
        var minutsList = ArrayList<Internet>()
        val db = FirebaseFirestore.getInstance()

        db.collection("uzmobile").document(language).collection("Internet")
            .addSnapshotListener { snapshot, error ->
                if (error != null) {
                    Log.w(TAG, "Listen Failed", error)
                    return@addSnapshotListener
                }
                if (snapshot != null) {
                    val documents = snapshot?.documents
                    documents?.forEach {
                        val minuts = it.toObject(Internet::class.java)
                        if (minuts != null) {
                            minutsList.add(minuts)
                        }
                    }
                    minuts.value = minutsList
                }

            }

        return minuts
    }

    fun getNews(language: String): MutableLiveData<ArrayList<News>> {
        var minuts = MutableLiveData<ArrayList<News>>()
        var minutsList = ArrayList<News>()
        val db = FirebaseFirestore.getInstance()

        db.collection("uzmobile").document(language).collection("Yangiliklar")
            .addSnapshotListener { snapshot, error ->
                if (error != null) {
                    Log.w(TAG, "Listen Failed", error)
                    return@addSnapshotListener
                }
                if (snapshot != null) {
                    val documents = snapshot?.documents
                    documents?.forEach {
                        val minuts = it.toObject(News::class.java)
                        if (minuts != null) {
                            minutsList.add(minuts)
                        }
                    }
                    minuts.value = minutsList
                }

            }

        return minuts
    }

    fun getAksiyalar(language: String): MutableLiveData<ArrayList<Aksiylar>> {
        var minuts = MutableLiveData<ArrayList<Aksiylar>>()
        var minutsList = ArrayList<Aksiylar>()
        val db = FirebaseFirestore.getInstance()

        db.collection("uzmobile").document(language).collection("Aksiyalar")
            .addSnapshotListener { snapshot, error ->
                if (error != null) {
                    Log.w(TAG, "Listen Failed", error)
                    return@addSnapshotListener
                }
                if (snapshot != null) {
                    val documents = snapshot?.documents
                    documents?.forEach {
                        val minuts = it.toObject(Aksiylar::class.java)
                        if (minuts != null) {
                            minutsList.add(minuts)
                        }
                    }
                    minuts.value = minutsList
                }

            }

        return minuts
    }

    fun getOpertorlar(language: String): MutableLiveData<ArrayList<Opetor>> {
        var minuts = MutableLiveData<ArrayList<Opetor>>()
        var minutsList = ArrayList<Opetor>()
        val db = FirebaseFirestore.getInstance()

        db.collection("uzmobile").document(language).collection("operator")
            .addSnapshotListener { snapshot, error ->
                if (error != null) {
                    Log.w(TAG, "Listen Failed", error)
                    return@addSnapshotListener
                }
                if (snapshot != null) {
                    val documents = snapshot?.documents
                    documents?.forEach {
                        val minuts = it.toObject(Opetor::class.java)
                        if (minuts != null) {
                            minutsList.add(minuts)
                        }
                    }
                    minuts.value = minutsList
                }

            }

        return minuts
    }

}