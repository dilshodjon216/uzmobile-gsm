package uz.dilshodjon216.uzmobileussd.model

import java.io.Serializable

data class Tariffs(
    var title:String="",
    var description:String="",
    var minutes:String="",
    var megabytes:String="",
    var sms: String="",
    var price:String="",
    var connectionCode:String="",
     var about:String=""):Serializable {

}