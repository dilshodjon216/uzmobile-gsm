package uz.dilshodjon216.uzmobileussd.model

data class Xizmatlar(
    var name: String = "",
    var about: String = "",
    var activCode: String = "",
    var price: String="",
    var noactivCode: String = "",
    var url:String=""
) {
}