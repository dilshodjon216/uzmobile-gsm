package uz.dilshodjon216.uzmobileussd.model

data class Ussd(var name: String = "", var code: String = "", var about: String = "")