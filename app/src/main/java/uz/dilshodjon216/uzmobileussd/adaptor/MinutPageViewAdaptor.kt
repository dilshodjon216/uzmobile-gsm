package uz.dilshodjon216.uzmobileussd.adaptor

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import uz.dilshodjon216.uzmobileussd.ui.fragment.ItemMinutFragment

@Suppress("DEPRECATION")
class MinutPageViewAdaptor(var nameTablayout:ArrayList<String>, fragmentManager: FragmentManager):
    FragmentStatePagerAdapter(fragmentManager) {
    override fun getCount(): Int =nameTablayout.size

    override fun getItem(position: Int): Fragment {
        return ItemMinutFragment()
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return nameTablayout[position]
    }
}