package uz.dilshodjon216.uzmobileussd.adaptor

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import uz.dilshodjon216.uzmobileussd.ui.fragment.ItemSMSFragment

@Suppress("DEPRECATION")
class SMSViewPageAdaptor(var smstitle: ArrayList<String>, fragmentManager: FragmentManager) :
    FragmentStatePagerAdapter(fragmentManager) {
    override fun getCount(): Int = smstitle.size

    override fun getItem(position: Int): Fragment {
        return ItemSMSFragment.newInstance(smstitle[position])
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return smstitle[position]
    }
}