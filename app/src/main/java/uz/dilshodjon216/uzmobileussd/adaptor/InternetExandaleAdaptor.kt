package uz.dilshodjon216.uzmobileussd.adaptor

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import uz.dilshodjon216.uzmobileussd.databinding.ItemChildInterntBinding
import uz.dilshodjon216.uzmobileussd.databinding.ItemGruopInternetBinding
import uz.dilshodjon216.uzmobileussd.model.Internet
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler1

class InternetExandaleAdaptor(var context: Context, var internetList: ArrayList<Internet>) :
    BaseExpandableListAdapter() {
    override fun getGroupCount(): Int = internetList.size

    override fun getChildrenCount(p0: Int): Int = 1

    override fun getGroup(p0: Int): Any = internetList[p0]

    override fun getChild(p0: Int, p1: Int): Any = internetList[p0]

    override fun getGroupId(p0: Int): Long = p0.toLong()

    override fun getChildId(p0: Int, p1: Int): Long = p1.toLong()

    override fun hasStableIds(): Boolean = true

    override fun getGroupView(p0: Int, p1: Boolean, p2: View?, p3: ViewGroup?): View {
        var binding = ItemGruopInternetBinding.inflate(LayoutInflater.from(context), p3, false)
        binding.internt = internetList[p0]
        return binding.root
    }

    override fun getChildView(p0: Int, p1: Int, p2: Boolean, p3: View?, p4: ViewGroup?): View {
        var binding = ItemChildInterntBinding.inflate(LayoutInflater.from(context), p4, false)
        var internt = getChild(p0, p1) as Internet
        binding.intnernt = internt
        var handler1 = MyHandler1()
        binding.handel = handler1
        return binding.root
    }

    override fun isChildSelectable(p0: Int, p1: Int): Boolean = true
}