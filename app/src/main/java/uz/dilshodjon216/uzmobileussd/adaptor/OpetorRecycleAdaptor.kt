package uz.dilshodjon216.uzmobileussd.adaptor

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.dilshodjon216.uzmobileussd.databinding.ItemOpeatorlarBinding
import uz.dilshodjon216.uzmobileussd.model.Opetor

class OpetorRecycleAdaptor(var context: Context, var operter: ArrayList<Opetor>) :
    RecyclerView.Adapter<OpetorRecycleAdaptor.VH>() {

    lateinit var binding: ItemOpeatorlarBinding

    inner class VH(view: View) : RecyclerView.ViewHolder(view) {
        fun onBinding(opetor: Opetor) {
            binding.opeator = opetor
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        binding = ItemOpeatorlarBinding.inflate(LayoutInflater.from(context), parent, false)
        return VH(binding.root)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.onBinding(operter[position])
    }

    override fun getItemCount(): Int = operter.size
}