package uz.dilshodjon216.uzmobileussd.adaptor

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.dilshodjon216.uzmobileussd.databinding.ItemMinutRvBinding
import uz.dilshodjon216.uzmobileussd.model.Minuts
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler

class ItemMinutRecycleAdaptor(var context: Context, var minutsList: ArrayList<Minuts>) :
    RecyclerView.Adapter<ItemMinutRecycleAdaptor.VH>() {
    lateinit var binding: ItemMinutRvBinding

    inner class VH(view: View) : RecyclerView.ViewHolder(view) {
        fun onBinding(minuts: Minuts) {
            var myHandler = MyHandler()
            binding.handler = myHandler
            binding.minut = minuts
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        binding = ItemMinutRvBinding.inflate(LayoutInflater.from(context), parent, false)
        return VH(binding.root)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.onBinding(minutsList[position])
    }

    override fun getItemCount(): Int = minutsList.size
}