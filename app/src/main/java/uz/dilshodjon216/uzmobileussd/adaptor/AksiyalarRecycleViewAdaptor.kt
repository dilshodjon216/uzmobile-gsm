package uz.dilshodjon216.uzmobileussd.adaptor

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.dilshodjon216.uzmobileussd.databinding.ItemAksiyalarBinding
import uz.dilshodjon216.uzmobileussd.model.Aksiylar
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler1

class AksiyalarRecycleViewAdaptor(var context: Context, var aksiylarList: ArrayList<Aksiylar>) :
    RecyclerView.Adapter<AksiyalarRecycleViewAdaptor.VH>() {

    lateinit var binding: ItemAksiyalarBinding

    inner class VH(view: View) : RecyclerView.ViewHolder(view) {
        fun binding(aksiylar: Aksiylar) {
            binding.aksiyalar=aksiylar
            var handler = MyHandler1()
            binding.handler = handler
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        binding = ItemAksiyalarBinding.inflate(LayoutInflater.from(context), parent, false)
        return VH(binding.root)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.binding(aksiylarList[position])
    }

    override fun getItemCount(): Int = aksiylarList.size
}