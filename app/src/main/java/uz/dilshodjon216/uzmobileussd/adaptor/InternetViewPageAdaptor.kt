package uz.dilshodjon216.uzmobileussd.adaptor

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import uz.dilshodjon216.uzmobileussd.ui.fragment.ItemInternetFragment

@Suppress("DEPRECATION")
class InternetViewPageAdaptor(
    var internetList: ArrayList<String>,
    fragmentManager: FragmentManager
) :
    FragmentStatePagerAdapter(fragmentManager) {
    override fun getCount(): Int = internetList.size

    override fun getItem(position: Int): Fragment {
        return ItemInternetFragment.newInstance(internetList[position])
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return internetList[position]
    }
}