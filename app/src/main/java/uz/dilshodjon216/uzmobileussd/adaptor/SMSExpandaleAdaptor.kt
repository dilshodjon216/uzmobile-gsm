package uz.dilshodjon216.uzmobileussd.adaptor

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import uz.dilshodjon216.uzmobileussd.databinding.ItemChildSmsBinding
import uz.dilshodjon216.uzmobileussd.databinding.ItemGruopSmsBinding
import uz.dilshodjon216.uzmobileussd.model.SMS
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler1

class SMSExpandaleAdaptor(var context: Context, var smsList: ArrayList<SMS>) :
    BaseExpandableListAdapter() {
    override fun getGroupCount(): Int = smsList.size

    override fun getChildrenCount(p0: Int): Int = 1

    override fun getGroup(p0: Int): Any = smsList[p0]

    override fun getChild(p0: Int, p1: Int): Any = smsList[p0]

    override fun getGroupId(p0: Int): Long = p0.toLong()

    override fun getChildId(p0: Int, p1: Int): Long = p1.toLong()

    override fun hasStableIds(): Boolean = true

    override fun getGroupView(p0: Int, p1: Boolean, p2: View?, p3: ViewGroup?): View {
        var biding = ItemGruopSmsBinding.inflate(LayoutInflater.from(context), p3, false)
        biding.sms = smsList[p0]

        return biding.root
    }

    override fun getChildView(p0: Int, p1: Int, p2: Boolean, p3: View?, p4: ViewGroup?): View {
        var biding = ItemChildSmsBinding.inflate(LayoutInflater.from(context), p4, false)
        var sms = getChild(p0, p1) as SMS
        biding.sms = sms
        var handler1=MyHandler1()
        biding.handel=handler1
        return biding.root
    }

    override fun isChildSelectable(p0: Int, p1: Int): Boolean = true
}