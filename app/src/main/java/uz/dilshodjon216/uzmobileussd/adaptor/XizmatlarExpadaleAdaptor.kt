package uz.dilshodjon216.uzmobileussd.adaptor

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import uz.dilshodjon216.uzmobileussd.databinding.ItemChildXizmatBinding
import uz.dilshodjon216.uzmobileussd.databinding.ItemGroupXizmatlarBinding
import uz.dilshodjon216.uzmobileussd.model.Xizmatlar
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler

class XizmatlarExpadaleAdaptor(var context: Context, var xizmatlarList: ArrayList<Xizmatlar>) :
    BaseExpandableListAdapter() {
    override fun getGroupCount(): Int = xizmatlarList.size
    override fun getChildrenCount(p0: Int): Int = 1

    override fun getGroup(p0: Int): Any = xizmatlarList[p0]

    override fun getChild(p0: Int, p1: Int): Any = xizmatlarList[p0]

    override fun getGroupId(p0: Int): Long = p0.toLong()

    override fun getChildId(p0: Int, p1: Int): Long = p1.toLong()

    override fun hasStableIds(): Boolean = true

    override fun getGroupView(p0: Int, p1: Boolean, p2: View?, p3: ViewGroup?): View {
        var binding = ItemGroupXizmatlarBinding.inflate(LayoutInflater.from(context), p3, false)
        binding.xizmatlar = xizmatlarList[p0]
        return binding.root
    }

    override fun getChildView(p0: Int, p1: Int, p2: Boolean, p3: View?, p4: ViewGroup?): View {
        var binding = ItemChildXizmatBinding.inflate(LayoutInflater.from(context), p4, false)
        var ussd = getChild(p0, p1) as Xizmatlar

        var handel = MyHandler()
        binding.handel = handel
        binding.xizmatlar = ussd
        return binding.root
    }

    override fun isChildSelectable(p0: Int, p1: Int): Boolean = true

}