package uz.dilshodjon216.uzmobileussd.adaptor

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import uz.dilshodjon216.uzmobileussd.ui.fragment.AksiyalarFragment
import uz.dilshodjon216.uzmobileussd.ui.fragment.ItemNewsFragment

@Suppress("DEPRECATION")
class NewsPageViewAdaptor(var title: ArrayList<String>, fragmentManager: FragmentManager) :
    FragmentStatePagerAdapter(fragmentManager) {
    override fun getCount(): Int = title.size

    override fun getItem(position: Int): Fragment {
        if (title[position] == "Yangiliklar" ||title[position]=="Новости"||title[position]=="Янгиликлар") {
            return ItemNewsFragment()
        } else {
            return AksiyalarFragment()
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return title[position]
    }
}