package uz.dilshodjon216.uzmobileussd.adaptor

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.dilshodjon216.uzmobileussd.databinding.ItemTarifBinding
import uz.dilshodjon216.uzmobileussd.model.Tariffs
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler

class TarffisRVAdaptor(var context: Context, var tariffList: ArrayList<Tariffs>) :
    RecyclerView.Adapter<TarffisRVAdaptor.VH>() {

    lateinit var binding1: ItemTarifBinding

    inner class VH(view: View) : RecyclerView.ViewHolder(view) {
        fun binding(tariffs: Tariffs) {
            var holder = MyHandler()
            binding1.tarffis = tariffs
            binding1.handler = holder
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        binding1 = ItemTarifBinding.inflate(LayoutInflater.from(context), parent, false)
        return VH(binding1.root)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.binding(tariffList[position])
    }

    override fun getItemCount(): Int = tariffList.size
}