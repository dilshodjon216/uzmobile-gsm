package uz.dilshodjon216.uzmobileussd.adaptor

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import uz.dilshodjon216.uzmobileussd.databinding.ItemPageviewBinding
import uz.dilshodjon216.uzmobileussd.model.Tariffs
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler

class BannerViewPageAdaptor(var context:Context,var tariffsList: ArrayList<Tariffs> ):PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val binding = ItemPageviewBinding.inflate(LayoutInflater.from(context), container, false)
        binding.tariffs = tariffsList[position]

        var handler= MyHandler()
        binding.handler=handler

        container?.addView(binding.root)
        return binding.root
    }
    override fun getCount(): Int=tariffsList.size

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}