package uz.dilshodjon216.uzmobileussd.adaptor

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import uz.dilshodjon216.uzmobileussd.databinding.ItemChildBinding
import uz.dilshodjon216.uzmobileussd.databinding.ItemEvUssdBinding
import uz.dilshodjon216.uzmobileussd.model.Ussd
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler

class UssdExpandaleAdaptor(var context: Context, var ussdList: ArrayList<Ussd>) :
    BaseExpandableListAdapter() {
    override fun getGroupCount(): Int = ussdList.size

    override fun getChildrenCount(p0: Int): Int = 1

    override fun getGroup(p0: Int): Any = ussdList[p0]

    override fun getChild(p0: Int, p1: Int): Any = ussdList[p0]

    override fun getGroupId(p0: Int): Long = p0.toLong()

    override fun getChildId(p0: Int, p1: Int): Long = p1.toLong()

    override fun hasStableIds(): Boolean = true

    override fun getGroupView(p0: Int, p1: Boolean, p2: View?, p3: ViewGroup?): View {
        var binding = ItemEvUssdBinding.inflate(LayoutInflater.from(context), p3, false)
        binding.ussd = ussdList[p0]
        return binding.root
    }

    override fun getChildView(p0: Int, p1: Int, p2: Boolean, p3: View?, p4: ViewGroup?): View {
        var binding = ItemChildBinding.inflate(LayoutInflater.from(context), p4, false)

        var ussd = getChild(p0, p1) as Ussd

        var handel = MyHandler()
        binding.handel = handel
        binding.ussd = ussd
        return binding.root
    }

    override fun isChildSelectable(p0: Int, p1: Int): Boolean = true

}