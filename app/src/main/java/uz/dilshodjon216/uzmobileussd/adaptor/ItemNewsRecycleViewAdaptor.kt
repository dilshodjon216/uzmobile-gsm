package uz.dilshodjon216.uzmobileussd.adaptor

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.dilshodjon216.uzmobileussd.databinding.ItemNewsBinding
import uz.dilshodjon216.uzmobileussd.model.News
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler1

class ItemNewsRecycleViewAdaptor(var context: Context, var newsList: ArrayList<News>) :
    RecyclerView.Adapter<ItemNewsRecycleViewAdaptor.VH>() {

    lateinit var binding: ItemNewsBinding

    inner class VH(view: View) : RecyclerView.ViewHolder(view) {
        fun binding(news: News) {
            binding.news = news
            var handler = MyHandler1()
            binding.handler = handler
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        binding = ItemNewsBinding.inflate(LayoutInflater.from(context), parent, false)
        return VH(binding.root)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.binding(newsList[position])
    }

    override fun getItemCount(): Int = newsList.size
}