package uz.dilshodjon216.uzmobileussd.ui.fragment

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import uz.dilshodjon216.uzmobileussd.R
import uz.dilshodjon216.uzmobileussd.databinding.FragmentBannerBinding
import uz.dilshodjon216.uzmobileussd.databinding.FragmentSettingsLanguageBinding
import uz.dilshodjon216.uzmobileussd.model.Tariffs
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler


class BannerFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentBannerBinding.inflate(inflater, container, false)
        setupPermissions()
        var tariffs=arguments?.getSerializable("tariffs") as Tariffs
        binding.tariffs=tariffs
        var handler= MyHandler()
        binding.handler=handler

        return binding.root
    }


    private fun setupPermissions() {
        if (ContextCompat.checkSelfPermission(requireContext(),
                Manifest.permission.CALL_PHONE)
            != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                    Manifest.permission.CALL_PHONE)) {
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(requireActivity(),
                    arrayOf(Manifest.permission.CALL_PHONE),
                    42)
            }
        } else {
            // Permission has already been granted

        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 42) {
            // If request is cancelled, the result arrays are empty.
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                // permission was granted, yay!

            } else {
            }
            return
        }
    }


}