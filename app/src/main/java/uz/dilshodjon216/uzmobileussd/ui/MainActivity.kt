package uz.dilshodjon216.uzmobileussd.ui

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.app.ShareCompat
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.Navigation
import com.google.android.material.navigation.NavigationView
import uz.dilshodjon216.uzmobileussd.R
import uz.dilshodjon216.uzmobileussd.localehelper.LocaleHelper

class   MainActivity : AppCompatActivity() {
    companion object {
        lateinit var mdrawerLayout: DrawerLayout
    }

    lateinit var actionBarDrawerToggle: ActionBarDrawerToggle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupPermissions()
        setContentView(R.layout.activity_main)
        mdrawerLayout = findViewById(R.id.drawer_layout)
        actionBarDrawerToggle =
            ActionBarDrawerToggle(this ,mdrawerLayout, R.string.open, R.string.close)
        mdrawerLayout.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.syncState()
      //  LocaleHelper().setLocale(this, "ru-rRU")
        findViewById<NavigationView>(R.id.nav_view).setNavigationItemSelectedListener{
            when (it.itemId) {
                R.id.nav_telegram -> {
                    var gourl = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.t.me/ussduz"))
                    startActivity(gourl)
                    return@setNavigationItemSelectedListener true
                }
                R.id.nav_aloqa -> {
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Biz bilan aloqa")
                    builder.setMessage("Email:dilshodjon216@gmail.com")
                    builder.setPositiveButton("xat jo'natish") { a, b ->
                        val emailIntent = Intent(
                            Intent.ACTION_SENDTO, Uri.fromParts(
                                "mailto", "dilshodjon216@gmail.com", null
                            )
                        )
                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject")
                        emailIntent.putExtra(Intent.EXTRA_TEXT, "Body")
                        startActivity(Intent.createChooser(emailIntent, "Send email..."))
                    }
                    builder.create().show()
                    return@setNavigationItemSelectedListener true
                }
                R.id.nav_ulashish -> {
                    ShareCompat.IntentBuilder.from(this)
                        .setType("text/plain")
                        .setChooserTitle("Chooser title")
                        .setText("Agar programmamni play marketga qo'ysam albatta yuklab ol")
                        .startChooser()
                    return@setNavigationItemSelectedListener true
                }
                R.id.nav_baholash -> {
                    var gourl = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=uz.pdp.uzmobile")
                    )
                    startActivity(gourl)
                    return@setNavigationItemSelectedListener true
                }
                else -> {
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Biz haqimizda")
                    builder.setMessage("Biz kim mulki Turon, amiri Turkistonmiz")
                    builder.create().show()
                    return@setNavigationItemSelectedListener true
                }
            }
        }

    }
    override fun onSupportNavigateUp(): Boolean {
        return Navigation.findNavController(this, R.id.nav_hostFragment).navigateUp()
    }

    private fun setupPermissions() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE)
            != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CALL_PHONE)) {
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.CALL_PHONE),
                    42)
            }
        } else {
            // Permission has already been granted

        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 42) {
            // If request is cancelled, the result arrays are empty.
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                // permission was granted, yay!

            } else {
            }
            return
        }
    }
}