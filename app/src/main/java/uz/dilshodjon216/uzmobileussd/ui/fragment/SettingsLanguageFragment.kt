package uz.dilshodjon216.uzmobileussd.ui.fragment

import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import uz.dilshodjon216.uzmobileussd.R
import uz.dilshodjon216.uzmobileussd.databinding.FragmentSettingsLanguageBinding
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.INSTALL
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.MyPREFERENCES
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.RU
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.UZB
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.UZB1

class SettingsLanguageFragment : Fragment() {

    lateinit var sharedpreferences: SharedPreferences
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentSettingsLanguageBinding.inflate(inflater, container, false)
        var handler= MyHandler()
        binding.uzb=UZB
        binding.ru= RU
        binding.uzb1= UZB1
        binding.handler=handler

        return binding.root
    }

    
}