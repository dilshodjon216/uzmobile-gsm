package uz.dilshodjon216.uzmobileussd.ui.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import uz.dilshodjon216.uzmobileussd.R
import uz.dilshodjon216.uzmobileussd.adaptor.InternetViewPageAdaptor
import uz.dilshodjon216.uzmobileussd.adaptor.SMSViewPageAdaptor
import uz.dilshodjon216.uzmobileussd.databinding.FragmentInternetBinding
import uz.dilshodjon216.uzmobileussd.modelView.BannerViewModel
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler
import uz.dilshodjon216.uzmobileussd.util.Constants


@Suppress("DEPRECATION")
class InternetFragment : Fragment() {


    lateinit var sharedpreferences: SharedPreferences
    lateinit var title: ArrayList<String>
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var binding=FragmentInternetBinding.inflate(inflater, container, false)

        var handler = MyHandler()
        binding.handler = handler
        sharedpreferences = activity?.getSharedPreferences(
            Constants.MyPREFERENCES,
            Context.MODE_PRIVATE
        )!!
        title = ArrayList()


        var language = sharedpreferences.getString(Constants.LANGUAGE, "")

        var aa = ""

        when (language) {
            Constants.UZB -> {
                aa = "uzb"
            }
            Constants.UZB1 ->{
                aa="uzb1"
            }
            Constants.RU ->{
                aa="ru"
            }
        }
        var type0=true
        var type1=true
        var type2=true
        var type3=true
        var bannerView = ViewModelProviders.of(this)[BannerViewModel::class.java]

        bannerView.listenToInternet(aa).observe(viewLifecycleOwner, Observer {
            for (a in it) {

                if(a.type=="0" && type0){
                    title.add(a.kind)
                    type0=false
                }
                if(a.type=="1" && type1){
                    title.add(a.kind)
                    type1=false
                }

                if(a.type=="2" && type2){
                    title.add(a.kind)
                    type2=false
                }
                if(a.type=="3" && type3){
                    title.add(a.kind)
                    type3=false
                }


            }
            var adaptor = InternetViewPageAdaptor(title, requireFragmentManager())
            binding.viewPagerNet.adapter = adaptor
        })




        binding.tablayoutNet.setupWithViewPager(binding.viewPagerNet)
        return binding.root
    }

    
}