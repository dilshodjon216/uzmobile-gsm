package uz.dilshodjon216.uzmobileussd.ui.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import uz.dilshodjon216.uzmobileussd.R
import uz.dilshodjon216.uzmobileussd.adaptor.ItemMinutRecycleAdaptor
import uz.dilshodjon216.uzmobileussd.modelView.BannerViewModel
import uz.dilshodjon216.uzmobileussd.util.Constants


class ItemMinutFragment : Fragment() {

    lateinit var sharedpreferences: SharedPreferences

    @SuppressLint("FragmentLiveDataObserve", "UseRequireInsteadOfGet")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var inflater = inflater.inflate(R.layout.fragment_item_minut, container, false)
        sharedpreferences = activity?.getSharedPreferences(
            Constants.MyPREFERENCES,
            Context.MODE_PRIVATE
        )!!
        var language = sharedpreferences.getString(Constants.LANGUAGE, "")

        var aa = ""

        when (language) {
            Constants.UZB -> {
                aa = "uzb"
            }
            Constants.UZB1 ->{
                aa="uzb1"
            }
            Constants.RU ->{
                aa="ru"
            }
        }

        var modelView = ViewModelProviders.of(this)[BannerViewModel::class.java]

        modelView.listenToMinuts(aa).observe(this, Observer {
            var adaptor = ItemMinutRecycleAdaptor(context!!, it)
            inflater.findViewById<RecyclerView>(R.id.minutRV).adapter = adaptor
        })


        return inflater
    }
    fun getPixelFromDips(pixels: Float): Int {
        // Get the screen's density scale
        val scale = resources.displayMetrics.density
        // Convert the dps to pixels, based on density scale
        return (pixels * scale + 0.5f).toInt()
    }

}