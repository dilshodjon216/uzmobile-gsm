package uz.dilshodjon216.uzmobileussd.ui.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.navigation.Navigation
import com.google.android.material.bottomnavigation.BottomNavigationView
import uz.dilshodjon216.uzmobileussd.R
import java.net.URLEncoder


@Suppress("DEPRECATION")
class HomeFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var inflater= inflater.inflate(R.layout.fragment_home, container, false)

        inflater.findViewById<BottomNavigationView>(R.id.bottomMenu).setOnNavigationItemSelectedListener(selectedListener)
        inflater.findViewById<CardView>(R.id.homeBtn).setOnClickListener {
            requireFragmentManager().beginTransaction().replace(
                R.id.nav_hostFragment1,
                DashboardFragment()
            ).commit()
        }
        return inflater
    }

    private val selectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.menu_balans->{
                    val intent = Intent(
                        Intent.ACTION_CALL,
                        Uri.parse("tel:" + URLEncoder.encode("*107#"))
                    )
                    requireView().context.startActivity(intent)
                }
                R.id.menu_operator->{
                    var a=this.toString()
                    requireFragmentManager().beginTransaction().replace(
                        R.id.nav_hostFragment1,
                        OperatlarFragment()
                    ).addToBackStack(a).commit()
                }
                R.id.menu_kabinet->{
                    var intent = Intent(Intent.ACTION_VIEW);
                    intent.data = Uri.parse("https://cabinet.uztelecom.uz/ps/scc/login.php")
                    requireContext().startActivity(intent)
                }
                R.id.menu_sozlamlar->{
                    Navigation.findNavController(requireView()).navigate(
                        R.id.action_homeFragment_to_settingsLanguageFragment
                    )
                }

            }
            false
        }


}