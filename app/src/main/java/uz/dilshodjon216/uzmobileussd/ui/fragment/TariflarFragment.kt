package uz.dilshodjon216.uzmobileussd.ui.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import uz.dilshodjon216.uzmobileussd.adaptor.TarffisRVAdaptor
import uz.dilshodjon216.uzmobileussd.databinding.FragmentTariflarBinding
import uz.dilshodjon216.uzmobileussd.modelView.BannerViewModel
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler
import uz.dilshodjon216.uzmobileussd.util.Constants


class TariflarFragment : Fragment() {

    lateinit var sharedpreferences: SharedPreferences

    @SuppressLint("FragmentLiveDataObserve")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var binding = FragmentTariflarBinding.inflate(inflater, container, false)

        sharedpreferences = activity?.getSharedPreferences(
            Constants.MyPREFERENCES,
            Context.MODE_PRIVATE
        )!!

        var aa = ""
        var language = sharedpreferences.getString(Constants.LANGUAGE, "")

        when (language) {
            Constants.UZB -> {
                aa = "uzb"
            }
            Constants.UZB1 ->{
                aa="uzb1"
            }
            Constants.RU ->{
                aa="ru"
            }
        }
        var holder = MyHandler()
        binding.handler = holder

        var bannerView = ViewModelProviders.of(this)[BannerViewModel::class.java]

        bannerView.listenToTariffs(aa, "Tariffs").observe(this, Observer {
            var bannerAdaptor = TarffisRVAdaptor(requireContext(), it)
            binding.RVTarifs.adapter = bannerAdaptor

        })

        return binding.root
    }


}