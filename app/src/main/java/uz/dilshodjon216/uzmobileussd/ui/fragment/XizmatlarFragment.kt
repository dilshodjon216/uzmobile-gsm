package uz.dilshodjon216.uzmobileussd.ui.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import uz.dilshodjon216.uzmobileussd.adaptor.UssdExpandaleAdaptor
import uz.dilshodjon216.uzmobileussd.adaptor.XizmatlarExpadaleAdaptor
import uz.dilshodjon216.uzmobileussd.databinding.FragmentXizmatlarBinding
import uz.dilshodjon216.uzmobileussd.modelView.BannerViewModel
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler
import uz.dilshodjon216.uzmobileussd.util.Constants


class XizmatlarFragment : Fragment() {

    lateinit var sharedpreferences: SharedPreferences
    @SuppressLint("FragmentLiveDataObserve")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var binding = FragmentXizmatlarBinding.inflate(inflater, container, false)
        sharedpreferences = activity?.getSharedPreferences(
            Constants.MyPREFERENCES,
            Context.MODE_PRIVATE
        )!!


        var handler = MyHandler()

        binding.handler = handler
        var language = sharedpreferences.getString(Constants.LANGUAGE, "")

        var aa = ""

        when (language) {
            Constants.UZB -> {
                aa = "uzb"
            }
            Constants.UZB1 ->{
                aa="uzb1"
            }
            Constants.RU ->{
                aa="ru"
            }
        }

        binding.expandXizmat.setOnGroupExpandListener(object :
            ExpandableListView.OnGroupExpandListener {
            var previousGroup: Int = -1
            override fun onGroupExpand(p0: Int) {
                if (p0 != previousGroup)
                    binding.expandXizmat.collapseGroup(previousGroup);
                previousGroup = p0;
            }
        })
        val metrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(metrics)
        val width = metrics.widthPixels
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            binding.expandXizmat.setIndicatorBounds(
                width - getPixelFromDips(50f),
                width - getPixelFromDips(10f)
            )

        } else {
            binding.expandXizmat.setIndicatorBoundsRelative(
                width - getPixelFromDips(50f),
                width - getPixelFromDips(10f)
            )

        }


        var modelView = ViewModelProviders.of(this)[BannerViewModel::class.java]

        modelView.listemToXizmatlar(aa).observe(this, Observer {
            var adaptor = XizmatlarExpadaleAdaptor(requireContext(), it)
            binding.expandXizmat.setAdapter(adaptor)
        })


        return binding.root
    }

    fun getPixelFromDips(pixels: Float): Int {
        // Get the screen's density scale
        val scale = resources.displayMetrics.density
        // Convert the dps to pixels, based on density scale
        return (pixels * scale + 0.5f).toInt()
    }

}