package uz.dilshodjon216.uzmobileussd.ui.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import uz.dilshodjon216.uzmobileussd.R
import uz.dilshodjon216.uzmobileussd.adaptor.BannerViewPageAdaptor
import uz.dilshodjon216.uzmobileussd.databinding.FragmentDashboardBinding
import uz.dilshodjon216.uzmobileussd.localehelper.LocaleHelper
import uz.dilshodjon216.uzmobileussd.model.Tariffs
import uz.dilshodjon216.uzmobileussd.modelView.BannerViewModel
import uz.dilshodjon216.uzmobileussd.ui.MainActivity
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler
import uz.dilshodjon216.uzmobileussd.ui.VerticalFlipTransformation
import uz.dilshodjon216.uzmobileussd.util.Constants
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.LANGUAGE
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.RU
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.TAG
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.UZB
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.UZB1


class DashboardFragment : Fragment() {
    lateinit var sharedpreferences: SharedPreferences
    var tariffs = ArrayList<Tariffs>()
    lateinit var handler: Handler
    var counter = 0
    lateinit var binding:FragmentDashboardBinding
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @SuppressLint("FragmentLiveDataObserve", "UseRequireInsteadOfGet")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        sharedpreferences = activity?.getSharedPreferences(
            Constants.MyPREFERENCES,
            Context.MODE_PRIVATE
        )!!

        var language = sharedpreferences.getString(LANGUAGE, "")

        var aa = ""

        when (language) {
            UZB -> {
                aa = "uzb"
            }
           UZB1->{
               aa="uzb1"
           }
           RU->{
               aa="ru"
           }
        }
        Log.w(Constants.TAG+"1",aa!!)

        binding = FragmentDashboardBinding.inflate(inflater, container, false)


        var verticalFli = VerticalFlipTransformation()
        binding.bannerPager.setPageTransformer(true, verticalFli)



        binding.tabLayout.setupWithViewPager(binding.bannerPager)
        var handler1= MyHandler()
        binding.handler=handler1
        handler = Handler()
        var bannerView = ViewModelProviders.of(this)[BannerViewModel::class.java]

        bannerView.listenToTariffs(aa, "Tariffs").observe(this, Observer {
            var bannerAdaptor = BannerViewPageAdaptor(context!!, it)
            binding.bannerPager.adapter = bannerAdaptor

        })

        binding.menuItem.setOnClickListener {
            MainActivity.mdrawerLayout.openDrawer(Gravity.LEFT)
        }



        Log.w(TAG+"11",LocaleHelper().getLanguage(context!!)!!)

        return binding.root
    }


    var runnable = object : Runnable {
        override fun run() {
            if (binding.bannerPager.currentItem == tariffs.size - 1) {
                counter = 0
            } else {
                counter++
            }
            binding.bannerPager.currentItem = counter
            handler.postDelayed(this, 3000)
        }
    }
}