package uz.dilshodjon216.uzmobileussd.ui.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.tabs.TabLayout
import uz.dilshodjon216.uzmobileussd.R
import uz.dilshodjon216.uzmobileussd.adaptor.SMSViewPageAdaptor
import uz.dilshodjon216.uzmobileussd.databinding.FragmentSmsBinding
import uz.dilshodjon216.uzmobileussd.modelView.BannerViewModel
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler1
import uz.dilshodjon216.uzmobileussd.util.Constants
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.TAG


@Suppress("DEPRECATION")
class SmsFragment : Fragment() {

    lateinit var sharedpreferences: SharedPreferences
    lateinit var title: ArrayList<String>

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var binding = FragmentSmsBinding.inflate(inflater, container, false)
        var handler = MyHandler()
        binding.handler = handler
        sharedpreferences = activity?.getSharedPreferences(
            Constants.MyPREFERENCES,
            Context.MODE_PRIVATE
        )!!
        title = ArrayList()


        var language = sharedpreferences.getString(Constants.LANGUAGE, "")

        var aa = ""

        when (language) {
            Constants.UZB -> {
                aa = "uzb"
            }
            Constants.UZB1 ->{
                aa="uzb1"
            }
            Constants.RU ->{
                aa="ru"
            }
        }
        var type0=true
        var type1=true
        var type2=true
        var bannerView = ViewModelProviders.of(this)[BannerViewModel::class.java]

        bannerView.listenToSMS(aa).observe(viewLifecycleOwner, Observer {
            for (a in it) {

                if(a.type=="0" && type0){
                    title.add(a.kind)
                    type0=false
                }
                if(a.type=="1" && type1){
                    title.add(a.kind)
                    type1=false
                }

                if(a.type=="2" && type2){
                    title.add(a.kind)
                    type2=false
                }


            }
            var adaptor = SMSViewPageAdaptor(title, fragmentManager!!)
            binding.viewPagerSms.adapter = adaptor
        })




        binding.tablayoutSms.setupWithViewPager(binding.viewPagerSms)
        val tabLayout = binding.tablayoutSms as TabLayout
        val betweenSpace = 30
        val slidingTabStrip = tabLayout.getChildAt(0) as ViewGroup
        for (i in 0 until slidingTabStrip.childCount - 1) {
            val v = slidingTabStrip.getChildAt(i)
            val params = v.layoutParams as ViewGroup.MarginLayoutParams
            params.rightMargin = betweenSpace
        }
        return binding.root
    }



}