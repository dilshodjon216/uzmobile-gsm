package uz.dilshodjon216.uzmobileussd.ui.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import uz.dilshodjon216.uzmobileussd.R
import uz.dilshodjon216.uzmobileussd.adaptor.AksiyalarRecycleViewAdaptor
import uz.dilshodjon216.uzmobileussd.adaptor.ItemNewsRecycleViewAdaptor
import uz.dilshodjon216.uzmobileussd.modelView.BannerViewModel
import uz.dilshodjon216.uzmobileussd.util.Constants


class AksiyalarFragment : Fragment() {


    lateinit var sharedpreferences: SharedPreferences
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var inflater=inflater.inflate(R.layout.fragment_aksiyalar, container, false)
        sharedpreferences = activity?.getSharedPreferences(
            Constants.MyPREFERENCES,
            Context.MODE_PRIVATE
        )!!
        var language = sharedpreferences.getString(Constants.LANGUAGE, "")

        var aa = ""

        when (language) {
            Constants.UZB -> {
                aa = "uzb"
            }
            Constants.UZB1 ->{
                aa="uzb1"
            }
            Constants.RU ->{
                aa="ru"
            }
        }

        var modelView = ViewModelProviders.of(this)[BannerViewModel::class.java]


        modelView.listenToAksiylar(aa).observe(viewLifecycleOwner, Observer {
            var adaptor = AksiyalarRecycleViewAdaptor(requireContext(), it)
            inflater.findViewById<RecyclerView>(R.id.aksiyaRV).adapter = adaptor
        })

        return inflater
    }


}