package uz.dilshodjon216.uzmobileussd.ui.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import uz.dilshodjon216.uzmobileussd.adaptor.NewsPageViewAdaptor
import uz.dilshodjon216.uzmobileussd.databinding.FragmentNewsBinding
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler
import uz.dilshodjon216.uzmobileussd.util.Constants
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.RU
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.UZB1


@Suppress("DEPRECATION")
class NewsFragment : Fragment() {

    lateinit var sharedpreferences: SharedPreferences
    lateinit var titleList: ArrayList<String>

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var binding = FragmentNewsBinding.inflate(inflater, container, false)
        sharedpreferences = activity?.getSharedPreferences(
            Constants.MyPREFERENCES,
            Context.MODE_PRIVATE
        )!!
        titleList = ArrayList()
        getTitle()

        var adaptor = NewsPageViewAdaptor(titleList, fragmentManager!!)
        binding.viewPagerNews.adapter = adaptor

        var handler = MyHandler()
        binding.handler = handler
        binding.tablayoutNews.setupWithViewPager(binding.viewPagerNews)



        return binding.root
    }

    private fun getTitle() {
        var language = sharedpreferences.getString(Constants.LANGUAGE, "")

        when (language) {
            Constants.UZB -> {
                titleList.add("Yangiliklar")
                titleList.add("Aksiyalar")
            }
            RU -> {
                titleList.add("Новости")
                titleList.add("Акции")
            }
            UZB1 -> {
                titleList.add("Янгиликлар")
                titleList.add("Акциялар")
            }
        }
    }


}