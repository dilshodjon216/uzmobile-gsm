package uz.dilshodjon216.uzmobileussd.ui.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import uz.dilshodjon216.uzmobileussd.R
import uz.dilshodjon216.uzmobileussd.adaptor.OpetorRecycleAdaptor
import uz.dilshodjon216.uzmobileussd.databinding.FragmentOperatlarBinding
import uz.dilshodjon216.uzmobileussd.modelView.BannerViewModel
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler
import uz.dilshodjon216.uzmobileussd.util.Constants


@Suppress("DEPRECATION")
class OperatlarFragment : Fragment() {

    lateinit var sharedpreferences: SharedPreferences
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var binding = FragmentOperatlarBinding.inflate(inflater, container, false)
        sharedpreferences = activity?.getSharedPreferences(
            Constants.MyPREFERENCES,
            Context.MODE_PRIVATE
        )!!


        var language = sharedpreferences.getString(Constants.LANGUAGE, "")

        var aa = ""

        when (language) {
            Constants.UZB -> {
                aa = "uzb"
            }
            Constants.UZB1 ->{
                aa="uzb1"
            }
            Constants.RU ->{
                aa="ru"
            }
        }

        var handler = MyHandler()
        binding.handler = handler

        var modelView = ViewModelProviders.of(this)[BannerViewModel::class.java]
        modelView.listenToOpeator(aa).observe(viewLifecycleOwner, Observer {
            var adaptor = OpetorRecycleAdaptor(requireContext(), it)
            binding.opeatorRV.adapter = adaptor
        })

        binding.arrow.setOnClickListener {
            requireFragmentManager().beginTransaction().replace(
                R.id.nav_hostFragment1,
                DashboardFragment()
            ).commit()
        }

        return binding.root
    }

}