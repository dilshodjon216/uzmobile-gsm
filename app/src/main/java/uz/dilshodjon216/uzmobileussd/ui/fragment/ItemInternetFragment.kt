package uz.dilshodjon216.uzmobileussd.ui.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import uz.dilshodjon216.uzmobileussd.R
import uz.dilshodjon216.uzmobileussd.adaptor.InternetExandaleAdaptor
import uz.dilshodjon216.uzmobileussd.adaptor.SMSExpandaleAdaptor
import uz.dilshodjon216.uzmobileussd.databinding.FragmentItemInternetBinding
import uz.dilshodjon216.uzmobileussd.model.Internet
import uz.dilshodjon216.uzmobileussd.model.SMS
import uz.dilshodjon216.uzmobileussd.modelView.BannerViewModel
import uz.dilshodjon216.uzmobileussd.util.Constants


class ItemInternetFragment : Fragment() {

    private  val ARG_PARAM1 = "param1"
    private var param1: String? = null
    lateinit var sharedpreferences: SharedPreferences
    lateinit var internetList:ArrayList<Internet>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)

        }
    }

    @SuppressLint("FragmentLiveDataObserve")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
     var binding=FragmentItemInternetBinding.inflate(inflater, container, false)
        sharedpreferences = activity?.getSharedPreferences(
            Constants.MyPREFERENCES,
            Context.MODE_PRIVATE
        )!!

        internetList=ArrayList()
        var language = sharedpreferences.getString(Constants.LANGUAGE, "")

        var aa = ""

        when (language) {
            Constants.UZB -> {
                aa = "uzb"
            }
            Constants.UZB1 ->{
                aa="uzb1"
            }
            Constants.RU ->{
                aa="ru"
            }
        }

        binding.expandInternet.setOnGroupExpandListener(object :
            ExpandableListView.OnGroupExpandListener {
            var previousGroup: Int = -1
            override fun onGroupExpand(p0: Int) {
                if (p0 != previousGroup)
                    binding.expandInternet.collapseGroup(previousGroup);
                previousGroup = p0;
            }
        })
        val metrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(metrics)
        val width = metrics.widthPixels
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            binding.expandInternet.setIndicatorBounds(
                width - getPixelFromDips(50f),
                width - getPixelFromDips(10f)
            )

        } else {
            binding.expandInternet.setIndicatorBoundsRelative(
                width - getPixelFromDips(50f),
                width - getPixelFromDips(10f)
            )

        }


        var modelView = ViewModelProviders.of(this)[BannerViewModel::class.java]

        modelView.listenToInternet(aa).observe(this, Observer {
            for( a in it){
                if (a.kind==param1){
                    internetList.add(a)
                }
            }
            var adaptor = InternetExandaleAdaptor(requireContext(), internetList)
            binding.expandInternet.setAdapter(adaptor)
        })
        return binding.root
    }

    fun getPixelFromDips(pixels: Float): Int {
        // Get the screen's density scale
        val scale = resources.displayMetrics.density
        // Convert the dps to pixels, based on density scale
        return (pixels * scale + 0.5f).toInt()
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String) =
            ItemInternetFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)

                }
            }
    }
}