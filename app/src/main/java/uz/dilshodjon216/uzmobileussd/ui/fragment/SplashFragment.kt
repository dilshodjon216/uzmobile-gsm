package uz.dilshodjon216.uzmobileussd.ui.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import uz.dilshodjon216.uzmobileussd.R
import uz.dilshodjon216.uzmobileussd.localehelper.LocaleHelper
import uz.dilshodjon216.uzmobileussd.util.Constants
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.RU
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.TAG
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.UZB
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.UZB1


class SplashFragment : Fragment() {

    lateinit var sharedpreferences: SharedPreferences
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_splash, container, false)
        sharedpreferences = activity?.getSharedPreferences(
            Constants.MyPREFERENCES,
            Context.MODE_PRIVATE
        )!!

        var installApp = sharedpreferences.getString(Constants.INSTALL, "")
        var lang = sharedpreferences.getString(Constants.LANGUAGE, "")


        if (installApp == "true") {

            when (lang) {
                UZB -> {
                    LocaleHelper().setLocale(view.context, "uz")
                }
                UZB1 -> {
                    LocaleHelper().setLocale(view.context, "uz-rUZ")
                }
                RU -> {
                    LocaleHelper().setLocale(view.context, "ru-rRU")
                }
            }
//
//            Log.w(TAG,lang!!)
//            LocaleHelper().setLocale(requireContext(), "ru-rRU")
            var handler = Handler()
            handler.postDelayed({
                val navOption = NavOptions.Builder().setPopUpTo(R.id.splashFragment, true).build()
                Navigation.findNavController(view).navigate(
                    R.id.action_splashFragment_to_homeFragment,
                    null,
                    navOption
                )
            }, 2000)
        } else {
            var handler = Handler()
            handler.postDelayed({
                val navOption = NavOptions.Builder().setPopUpTo(R.id.splashFragment, true).build()
                Navigation.findNavController(view).navigate(
                    R.id.action_splashFragment_to_settingsLanguageFragment,
                    null,
                    navOption
                )
            }, 2000)
        }
        return view
    }


}