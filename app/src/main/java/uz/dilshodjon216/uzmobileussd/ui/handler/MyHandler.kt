package uz.dilshodjon216.uzmobileussd.ui.handler


import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.app.ShareCompat
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import uz.dilshodjon216.uzmobileussd.R
import uz.dilshodjon216.uzmobileussd.databinding.LayoutDialogBinding
import uz.dilshodjon216.uzmobileussd.localehelper.LocaleHelper
import uz.dilshodjon216.uzmobileussd.model.Minuts
import uz.dilshodjon216.uzmobileussd.model.Tariffs
import uz.dilshodjon216.uzmobileussd.model.Ussd
import uz.dilshodjon216.uzmobileussd.ui.fragment.OperatlarFragment
import uz.dilshodjon216.uzmobileussd.util.Constants
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.INSTALL
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.LANGUAGE
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.RU
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.UZB
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.UZB1
import java.net.URLEncoder


@Suppress("DEPRECATION")
class MyHandler {
    fun languageButtons(view: View, text: String) {
        var sharedpreferences =
            view.context.getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE)!!
        var editor = sharedpreferences.edit()
        when (text) {
            UZB -> {
                editor.putString(INSTALL, "true")
                editor.putString(LANGUAGE, UZB)
                editor.apply()
                LocaleHelper().setLocale(view.context, "uz-rUZ")

            }
            RU -> {
                editor.putString(INSTALL, "true")
                editor.putString(LANGUAGE, RU)
                editor.apply()
                    LocaleHelper().setLocale(view.context, "ru-rRU")
            }
            UZB1 -> {
                editor.putString(INSTALL, "true")
                editor.putString(LANGUAGE, UZB1)
                editor.apply()
                LocaleHelper().setLocale(view.context, "uz")
            }
        }
        val navOption = NavOptions.Builder().setPopUpTo(R.id.settingsLanguageFragment, true).build()
        Navigation.findNavController(view).navigate(
            R.id.action_settingsLanguageFragment_to_homeFragment,
            null,
            navOption
        )
    }

    fun printBanner(view: View, tariffs: Tariffs) {
        val bundle = Bundle()
        bundle.putSerializable("tariffs", tariffs)
        Navigation.findNavController(view).navigate(
            R.id.action_dashboardFragment_to_bannerFragment,
            bundle
        )
    }

    fun bannerBackFragment(view: View) {
        view.findNavController().popBackStack()
    }

    fun bannerBackFragment1(view: View) {

       // var manager=FragmentManager()
    }

    fun bannerCallClick(view: View, string: Tariffs) {
        // Toast.makeText(view.context,"ssss",Toast.LENGTH_SHORT).show()
        val intent = Intent(
            Intent.ACTION_CALL,
            Uri.parse("tel:" + URLEncoder.encode(string.connectionCode))
        )
        view.context.startActivity(intent)
    }

    fun bannerUrlCallClick(view: View, string: Tariffs) {
        var intent = Intent(Intent.ACTION_VIEW);
        intent.data = Uri.parse(string.about)
        view.context.startActivity(intent)
    }

    fun openNewsFragment(view: View) {
        Navigation.findNavController(view).navigate(
            R.id.action_dashboardFragment_to_newsFragment
        )
    }

    fun openTelegramChannel(view: View) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/ussduz"))
        view.context.startActivity(intent)
    }


    fun shareitUrl(view: View) {
        ShareCompat.IntentBuilder.from(view.context as Activity)
            .setType("text/plain")
            .setChooserTitle("Chooser title")
            .setText("Agar programmamni play marketga qo'ysam albatta yuklab ol")
            .startChooser();

    }

    fun openUSSDFragment(view: View) {
        Navigation.findNavController(view).navigate(
            R.id.action_dashboardFragment_to_ussdFragment
        )
    }

    fun ussdCallClick(view: View, string: Ussd) {
        // Toast.makeText(view.context,"ssss",Toast.LENGTH_SHORT).show()
        val intent = Intent(
            Intent.ACTION_CALL,
            Uri.parse("tel:" + URLEncoder.encode(string.code))
        )
        view.context.startActivity(intent)
    }

    fun onClickTarif(view: View) {
        val intent = Intent(
            Intent.ACTION_CALL,
            Uri.parse("tel:" + URLEncoder.encode("*107#"))
        )
        view.context.startActivity(intent)
    }

    fun openTariffs(view: View) {
        Navigation.findNavController(view).navigate(
            R.id.action_dashboardFragment_to_tariflarFragment
        )
    }

    fun printBanner1(view: View, tariffs: Tariffs) {
        val bundle = Bundle()
        bundle.putSerializable("tariffs", tariffs)
        Navigation.findNavController(view).navigate(
            R.id.action_tariflarFragment_to_bannerFragment,
            bundle
        )
    }

    fun printXizmat(view: View) {
        Navigation.findNavController(view).navigate(
            R.id.action_dashboardFragment_to_xizmatlarFragment
        )
    }

    fun printMinut(view: View) {
        Navigation.findNavController(view).navigate(
            R.id.action_dashboardFragment_to_minutFragment
        )
    }
    fun printInternet(view:View){
        Navigation.findNavController(view).navigate(
            R.id.action_dashboardFragment_to_internetFragment
        )
    }

    fun openMenuShitte(view: View,minuts: Minuts) {
        var builder = AlertDialog.Builder(view.context)
        var binding = LayoutDialogBinding.inflate(LayoutInflater.from(view.context), view.findViewById(R.id.content),false)
        binding.minut=minuts
        builder.setView(binding.root)
        var dialog=builder.create()
        dialog.show()

    }

    fun clickMinutAktivishon(view: View,mints:Minuts){
        val intent = Intent(
            Intent.ACTION_CALL,
            Uri.parse("tel:" + URLEncoder.encode(mints.code))
        )
        view.context.startActivity(intent)
    }

    fun clickMinutShearit(view: View,mints:Minuts){
        ShareCompat.IntentBuilder.from(view.context as Activity)
            .setType("text/plain")
            .setChooserTitle(mints.code)
            .setText(mints.code)
            .startChooser();
    }

    fun printSMSFragment(view: View){
        Navigation.findNavController(view).navigate(
            R.id.action_dashboardFragment_to_smsFragment
        )
    }


}