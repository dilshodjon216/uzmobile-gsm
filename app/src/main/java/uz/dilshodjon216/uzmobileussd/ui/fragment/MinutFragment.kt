package uz.dilshodjon216.uzmobileussd.ui.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import uz.dilshodjon216.uzmobileussd.adaptor.MinutPageViewAdaptor
import uz.dilshodjon216.uzmobileussd.databinding.FragmentMinutBinding
import uz.dilshodjon216.uzmobileussd.ui.handler.MyHandler
import uz.dilshodjon216.uzmobileussd.util.Constants
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.RU
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.UZB
import uz.dilshodjon216.uzmobileussd.util.Constants.Companion.UZB1


@Suppress("DEPRECATION")
class MinutFragment : Fragment() {

    lateinit var title: ArrayList<String>
    lateinit var sharedpreferences: SharedPreferences
    var language: String? = ""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var biding = FragmentMinutBinding.inflate(inflater, container, false)
        sharedpreferences = activity?.getSharedPreferences(
            Constants.MyPREFERENCES,
            Context.MODE_PRIVATE
        )!!
        language = sharedpreferences.getString(Constants.LANGUAGE, "")
        loadTitle()
        val tabLayout = biding.tablayoutMinut
        val betweenSpace = 30

        val slidingTabStrip = tabLayout.getChildAt(0) as ViewGroup

        for (i in 0 until slidingTabStrip.childCount - 1) {
            val v = slidingTabStrip.getChildAt(i)
            val params = v.layoutParams as ViewGroup.MarginLayoutParams
            params.rightMargin = betweenSpace
        }

        var adaptor = MinutPageViewAdaptor(title, requireFragmentManager())
        biding.viewPagerSms.adapter = adaptor

        biding.tablayoutMinut.setupWithViewPager(biding.viewPagerSms)

        var myHolder = MyHandler()
        biding.handler = myHolder


        return biding.root
    }

    private fun loadTitle() {
        title = ArrayList()
        when (language) {
            UZB -> {
                title.add("Daqiqa to'plamlar")
                // title.add("Daqiqa to'plamlar")

            }
            RU -> {
                title.add("Минутные пакеты")
            }
            UZB1 -> {
                title.add("Дақиқа тўпламлар")
            }
        }

    }


}