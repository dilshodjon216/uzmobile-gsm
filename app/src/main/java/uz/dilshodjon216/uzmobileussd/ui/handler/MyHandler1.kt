package uz.dilshodjon216.uzmobileussd.ui.handler


import android.content.Intent
import android.net.Uri
import android.view.View
import uz.dilshodjon216.uzmobileussd.model.Aksiylar
import uz.dilshodjon216.uzmobileussd.model.Internet
import uz.dilshodjon216.uzmobileussd.model.News
import uz.dilshodjon216.uzmobileussd.model.SMS

import java.net.URLEncoder

@Suppress("DEPRECATION")
class MyHandler1 {

    fun onClickSMSAbout(view: View, sms: SMS) {
        var intent = Intent(Intent.ACTION_VIEW);
        intent.data = Uri.parse(sms.url)
        view.context.startActivity(intent)
    }

    fun onCliCkActivition(view: View, sms: SMS) {
        val intent = Intent(
            Intent.ACTION_CALL,
            Uri.parse("tel:" + URLEncoder.encode(sms.activeCod))
        )
        view.context.startActivity(intent)
    }

    fun onClickInterntAbout(view: View, internet: Internet) {
        var intent = Intent(Intent.ACTION_VIEW);
        intent.data = Uri.parse(internet.url)
        view.context.startActivity(intent)
    }

    fun onCliCkInternetActivition(view: View, internet: Internet) {
        val intent = Intent(
            Intent.ACTION_CALL,
            Uri.parse("tel:" + URLEncoder.encode(internet.activeCod))
        )
        view.context.startActivity(intent)
    }

    fun openWebClcik(view: View, news: News) {
        var intent = Intent(Intent.ACTION_VIEW);
        intent.data = Uri.parse(news.url)
        view.context.startActivity(intent)
    }

    fun openWebClcik1(view: View,aksiylar: Aksiylar) {
        var intent = Intent(Intent.ACTION_VIEW);
        intent.data = Uri.parse(aksiylar.url)
        view.context.startActivity(intent)
    }
}